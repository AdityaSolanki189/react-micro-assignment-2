import React, { useState } from 'react';
import makeAnimated from 'react-select/animated';
import Creatable from 'react-select/creatable';
import { useWindowSize } from '@react-hook/window-size';
import Confetti from 'react-confetti';

import './App.css';

const animatedComponents = makeAnimated();

function App() {

    const [width, height] = useWindowSize();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [submit, setSubmit] = useState(false);

    const [selectedOption, setSelectedOption] = useState(null);

    const skillSets = [
        { value: 'opt1', label: 'JavaScript' },
        { value: 'opt2', label: 'ReactJs' },
        { value: 'opt3', label: 'NodeJs' },
        { value: 'opt4', label: 'MongoDB' },
        { value: 'opt5', label: 'ExpressJs' },
        { value: 'opt8', label: 'Bootstrap' },
        { value: 'opt9', label: 'Material UI' },
        { value: 'opt10', label: 'React Native' },
        { value: 'opt11', label: 'Redux' },
        { value: 'opt12', label: 'Git' },
        { value: 'opt14', label: 'Firebase' },
        { value: 'opt15', label: 'Heroku' },     
        { value: 'opt16', label: 'Django' },
        { value: 'opt17', label: 'Python' },
        { value: 'opt20', label: 'MySQL' },
        { value: 'opt21', label: 'PostgreSQL' },
        { value: 'opt22', label: 'GraphQL' },
        { value: 'opt23', label: 'REST API' },
        { value: 'opt25', label: 'Next.js' },
    ];

    const onFormSubmitHandler = (e) => {

        e.preventDefault();
        setSubmit(true);
    }

    return (
        <div className="App">
            <div className="main-container">

                

                <div className="left-container">

                    <p className='title-top'>Learn to code by watching others</p>

                    <span className='title-bottom'>
                        See how experienced developers solve problems in real-time. <br/>
                        Watching scripted tutorials is great, but understanding how developers think is invaluable.
                    </span>

                </div>

                <div className="right-container">

                    <div className="top-message">
                        {submit ? 
                            <p className="top-message-title">You have successfully subscribed to our plan ✔️</p> 
                            : 
                            <p className="top-message-title">
                            <strong>Try it free 7 days</strong> then ₹180/mo. thereafter
                            </p>
                        }
                        

                    </div>

                    <div className="form-container">
                        <form className='form'>
                            <input type="text" placeholder="Your Full Name" className="input-field" onChange={(e) => { setName(e.target.value )}} required/>
                            <input type="text" placeholder="Email Address" className="input-field" onChange={(e) => { setEmail(e.target.value )}} required/>
                            <input type="password" placeholder="Password" className="input-field" onChange={(e) => { setPassword(e.target.value )}} required/>
                            
                            <Creatable 
                                placeholder="Choose your Skills"
                                className="select-field"
                                closeMenuOnSelect={false}
                                components={animatedComponents}
                                required
                                isMulti
                                onChange={setSelectedOption}
                                options={skillSets}
                            />
                            
                            {name && email && password && selectedOption?
                                <button className="submit-button-enabled" onClick={onFormSubmitHandler}>CLAIM YOUR FREE TRIAL</button>    
                                :
                                <button className="submit-button">CLAIM YOUR FREE TRIAL</button>
                            }

                            <p className="bottom-message">
                                By clicking the button, you are agreeing to our <a href="#">Terms and Services</a>
                            </p>
                        </form>
                        
                        {submit ? <Confetti width={width-10} height={height-10} /> : null
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;